<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Ticket;

class TicketController extends Controller
{
    public function create (Response $response, Request $request)
    {

        $request->validate([
            'title' => 'required|string|max:255'
        ]);

        $user = Auth::user();
        $tiket = new Ticket;
        $tiket->title = $request->title;
        $tiket->assignee =  $user->id;
        $tiket->status = 'on-hold';
        $tiket->save();

        return [
            'status'=> $response->status(),
            'ticket' => $tiket
        ];
    }

    public function allTickets (Response $response)
    {

        $tikets =  Ticket::all();

        return [
            'status'=> $response->status(),
            'tickets' => $tikets
        ];
    }

    public function userTickets (Response $response)
    {
        $user = Auth::user();
        $tikets =   $user->userTikets;

        return [
            'status'=> $response->status(),
            'tickets' => $tikets
        ];
    }

    public function agentTickets (Response $response)
    {
        $user = Auth::user();
        $tikets =   $user->agentTikets;

        return [
            'status'=> $response->status(),
            'tickets' => $tikets
        ];
    }

    public function ticket (Response $response, Ticket $ticket)
    {
        $ticket->comments;

        return [
            'status'=> $response->status(),
            'ticket' => $ticket
        ];
    }

    public function ticketDelete (Response $response, Ticket $ticket)
    {
        $user = Auth::user();
        if ( $user->role == 'agent') {
            $ticket->delete();
        }
        return [
            'status'=> $response->status(),
            'ticket' => $ticket
        ];
    }


    public function ticketChangeStatus (Response $response,  Request $request, Ticket $ticket)
    {
        $request->validate([
            'status' => 'required|string|max:255'
        ]);

        $user = Auth::user();
        if ( $user->role == 'agent' || $user->id == $ticket->assignee) {
            $ticket->status =  $request->status;
            $ticket->reporter =  $user->id;
            $ticket->update();
        }
        return [
            'status'=> $response->status(),
            'ticket' => $ticket
        ];
    }

    public function ticketUpdate (Response $response,  Request $request, Ticket $ticket)
    {
        $request->validate([
            'title' => 'required|string|max:255'
        ]);

        $user = Auth::user();
        if ( $user->id == $ticket->assignee) {
            $ticket->title =  $request->title;
            $ticket->update();
        }
        return [
            'status'=> $response->status(),
            'ticket' => $ticket
        ];
    }
}
