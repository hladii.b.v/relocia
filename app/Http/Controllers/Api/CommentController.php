<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Ticket;
use App\Comment;

class CommentController extends Controller
{
    public function createUserComment (Response $response, Request $request, Ticket $ticket)
    {

        $request->validate([
            'contents' => 'required|string|max:255'
        ]);

        $user = Auth::user();
        $comment = new Comment;
        $comment->ticket_id = $ticket->id;
        $comment->user_id =  $user->id;
        $comment->contents = $request->contents;
        $comment->type = 'publick';
        $comment->save();

        return [
            'status'=> $response->status(),
            'comment' => $comment
        ];
    }

    public function createAgentComment (Response $response, Request $request, Ticket $ticket)
    {

        $request->validate([
            'contents' => 'required|string|max:255',
            'type' => 'required|string|max:255'
        ]);

        $user = Auth::user();

        if ($user->role == 'agent') {
            $comment = new Comment;
            $comment->ticket_id = $ticket->id;
            $comment->user_id =  $user->id;
            $comment->contents = $request->contents;
            $comment->type = $request->type;;
            $comment->save();

            return [
                'status'=> $response->status(),
                'comment' => $comment
            ];
        }
    }


}
