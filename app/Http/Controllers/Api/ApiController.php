<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;


use App\User;

class ApiController extends Controller
{
    public function createUser (Response $response, Request $request)
    {


        $request->validate([
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:8',
        ]);

        $email = $request->email;
        $name = $request->name;
        $password = $request->password;
        $token = Str::random(80);

        $user = User::create([
            'email' => $email,
            'name' => $name,
            'role' => 'customer',
            'password' => Hash::make($password),
            'api_token' => hash('sha256', $token),
        ]);


        return [
            'status'=> $response->status(),
            'user' => $user,
            'token' => $token
        ];

    }

    public function getToken(Response $response, Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ( $user) {
            if (Hash::check($request->password, $user->password)) {

                $token = Str::random(80);
                $user->api_token = hash('sha256', $token);
                $user->update();

                return [
                    'status'=> $response->status(),
                    'user' => $user,
                    'token' => $token
                ];
            }
            else {
                return $response->setStatusCode(404, 'Wrong password');
            }
        }
        else {
            return $response->setStatusCode(404, 'Wrong email');
        }
    }

    public function logout (Response $response)
    {
        $user = Auth::user();
        if ($user) {

            $user->api_token = Null;
            $user->update();
            return [
                'status'=>  $response->status()
            ];
        }
        else {
            return $response->setStatusCode(404, 'Wrong token');
        }
    }

}
