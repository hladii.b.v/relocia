<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model
{
    // public function metaElenents()
    // {
    // 	return $this->hasMany('App\TemplateMeta')->orderBy('meta_key');
    // }

    public function assignee()
    {
    	return $this->belongsTo('App\User', 'assignee', 'id');
    }

    public function reporter()
    {
    	return $this->belongsTo('App\User', 'reporter', 'id');
    }

    public function comments()
    {
        $user = Auth::user();
        if ($user->role == 'agent') {
            return $this;
            return $this->hasMany('App\Comment')->where('type', 'publick');
        }
        else {
            return $this->hasMany('App\Comment', 'ticket_id', 'id');
        }
    }

}
