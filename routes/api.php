<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'prefix' => 'v1'], function() {

    Route::post('/create-user','ApiController@createUser');

    Route::post('/getToken','ApiController@getToken');

    Route::group(['middleware' => 'auth:api'], function () {

        Route::post('/logout',  'ApiController@logout');

        Route::post('/create-ticket',  'TicketController@create');

        Route::get('/tickets',  'TicketController@allTickets');

        Route::get('/user-tickets',  'TicketController@userTickets');

        Route::get('/agent-tickets',  'TicketController@agentTickets');

        Route::get('/ticket/{ticket}',  'TicketController@ticket');

        Route::post('/ticket-delete/{ticket}',  'TicketController@ticketDelete');

        Route::post('/ticket-status/{ticket}',  'TicketController@ticketChangeStatus');

        Route::post('/ticket-update/{ticket}',  'TicketController@ticketUpdate');

        Route::post('/create-comment/{ticket}',  'CommentController@createUserComment');

        Route::post('/create-agent-comment/{ticket}',  'CommentController@createAgentComment');

    });
});
