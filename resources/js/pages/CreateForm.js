import React, { useState } from 'react';
import styled from 'styled-components';

const TodoForm = () => {
  const [value, setValue] = useState('');

    const onInputChange = ({target}) => {
        setValue(target.value);
      }

    const fetchProducts = () => {

    }

    const Input = styled.input`
        background: #000
    `

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
      }}
    >
      <Input
        variant="outlined"
        placeholder="Add todo"
        margin="normal"
        onChange={onInputChange}
        value={value}
      />
    </form>
  );
};

export default TodoForm;
