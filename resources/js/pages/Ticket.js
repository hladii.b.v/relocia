import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import styled from 'styled-components';

const Ticket = () => {

    const [ticket, setTicket] = useState({});
    const {id} = useParams();
    useEffect(() => {
        axios.get(`http://relocia/api/v1/ticket/${id}`, {
            headers: {
                Authorization : 'Bearer sNUpT2yNJY2ys2yADsuoPB3tL09zGv5Wth1IlvPMNYgzNDmvP178F3MKTON5vKtPuIXfGw9AbZALzwBd'
            }
        }).then((res)  => {
            setTicket (res.data.ticket)
        })
    }, [])
    const Container = styled.div `
        font-size: 15px;
        text-align: center;
        color: palevioletred;
        padding: 20px;
        display: flex;
        flex-direction: column;
        align-items: center;
        font-weight: 700;
    }
        `;

    const Row = styled.div `
        display: flex;
        padding: 15px;
        width: 30%;
        min-width: 320px;
        justify-content: space-between;
        `;

    const Title = styled.span `
        color: #1e1e6f;
        `;

    const Button = styled.button `
        color: #1e1e6f;
        color: #1e1e6f;
        background-color: red;
        border-radius: 8px;
        padding: 5px 10px;

        `;
    return (
        <Container >
            <h3>{ticket.title}</h3>
            {ticket.comments?.map( (comment)=>{

                return <Row >
                        <Title> {comment.contents} </Title>
                    </Row>
            })}
        </Container>
    );
};

export default Ticket;
