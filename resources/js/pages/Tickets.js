import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { useForm } from "react-hook-form";

const Tickets = () => {


    const [tickets, setTickets] = useState([]);

    useEffect(() => {
        axios.get('http://relocia/api/v1/tickets', {
            headers: {
                Authorization : 'Bearer sNUpT2yNJY2ys2yADsuoPB3tL09zGv5Wth1IlvPMNYgzNDmvP178F3MKTON5vKtPuIXfGw9AbZALzwBd'
            }
        }).then((res)  => {
            setTickets (res.data.tickets)
        })
    }, [])

    const [form, setForm] = useState({
        Name: ''
    });

    const handleChange = event => {
        // use spread operator
        setForm({
          ...form,
          [event.target.name]: event.target.value,
        });
    };
    const handleSubmit = event =>  {
        axios.post('http://relocia/api/v1/tickets', {
            headers: {
                Authorization : 'Bearer sNUpT2yNJY2ys2yADsuoPB3tL09zGv5Wth1IlvPMNYgzNDmvP178F3MKTON5vKtPuIXfGw9AbZALzwBd'
            }
        }).then((res)  => {
            setTickets (res.data.tickets)
        })
    }

    return (
        <Container >
            <form  onSubmit={handleSubmit}>
                <label>
                    Ім'я:</label>
                    <input type="text" name="Name" value={form.Name} onChange={handleChange}/>

                <input type="submit" value="Надіслати" />
            </form>

            <h3>Your tickets</h3>
            {tickets.map( (ticket)=>{

                return <Row >
                        <Link to={`/ticket/${ticket.id}`} key={ticket.id}>
                            <Title> {ticket.title} </Title>
                        </Link>
                        <Button> Close Ticket </Button>
                    </Row>
            })}
        </Container>
    );
};

const Container = styled.div `
display: block;
width: 400px;
margin: 100px auto 0;
background: #fff;
color: #333;
font-family: Lato, sans-serif;
`;

const Row = styled.div `
display: flex;
padding: 15px;
width: 30%;
min-width: 320px;
justify-content: space-between;
`;

const Title = styled.span `
color: #1e1e6f;
`;

const Button = styled.button `
color: #1e1e6f;
color: #1e1e6f;
background-color: red;
border-radius: 8px;
padding: 5px 10px;

`;

export default Tickets;
