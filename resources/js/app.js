import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Create from "./pages/CreateForm";
import Tickets from "./pages/Tickets";
import Ticket from "./pages/Ticket";

export default function App() {
    return (
      <Router>
        <div>
          {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/create">
              <Create />
            </Route>
            <Route path="/tickets">
              <Tickets />
            </Route>
            <Route path="/ticket/:id">
              <Ticket />
            </Route>
            {/* <Route path="/tickets">
              <Tickets />
            </Route> */}
          </Switch>
        </div>
      </Router>
    );
  }



if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}
